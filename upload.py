#!/usr/bin/env python3


import glob
import json
import os
from typing import Dict
import unicodedata

import flickrapi
import pkg_resources
from pyckson import parse, serialize
from retrying import retry
import tqdm


API_KEY = ''
API_SECRET = ''

PHOTO_DIRECTORY = ''
PHOTOSET_TITLE = ''

PROGRESS_PATH = 'progress.json'
OUTPUT_PATH = 'output.json'
KEYS_TO_SAVE = ('url_o', 'url_sq',)


LOGO_PATH = pkg_resources.resource_filename(
    __name__,
    os.path.join('resources', 'logo.jpg')
)


class Photo:
    def __init__(self, photo_id: str = None, in_photoset: bool = False):
        self.photo_id = photo_id
        self.in_photoset = in_photoset


class State:
    def __init__(
            self,
            logo_id: str = None,
            photoset_id: str = None,
            photos: Dict[str, Photo] = None
    ):
        self.logo_id = logo_id
        self.photoset_id = photoset_id
        self.photos = photos if photos is not None else {}


def get_all_files():
    return list(filter(
        os.path.isfile,
        glob.iglob(PHOTO_DIRECTORY + '/**/*.*', recursive=True)
    ))


def get_name(filepath):
    return unicodedata.normalize('NFC', os.path.splitext(os.path.basename(filepath))[0].strip())


def load_progress():
    return parse(State, json.load(open(PROGRESS_PATH)))


def dump_progress(state):
    json.dump(serialize(state), open(PROGRESS_PATH, 'w'), ensure_ascii=False)


def upload_photo(flickr, file_path, name):
    rsp = flickr.upload(
        file_path,
        title=name,
        is_public=0,
        is_family=0,
        is_friend=0,
        format='etree',
    )
    assert rsp.attrib['stat'] == 'ok'
    return rsp[0].text


@retry(stop_max_attempt_number=10, wait_fixed=1000)
def main():
    flickr = flickrapi.FlickrAPI(API_KEY, API_SECRET)
    flickr.authenticate_via_browser(perms='write')

    if os.path.exists(PROGRESS_PATH):
        state = load_progress()
    else:
        state = State()

    if state.logo_id is None:
        assert os.path.isfile(LOGO_PATH)
        state.logo_id = upload_photo(
            flickr,
            LOGO_PATH,
            '{} logo'.format(PHOTOSET_TITLE)
        )
        dump_progress(state)

    if state.photoset_id is None:
        rsp = json.loads(flickr.photosets.create(
            title=PHOTOSET_TITLE,
            primary_photo_id=state.logo_id,
            format='json',
        ))
        assert rsp['stat'] == 'ok'
        state.photoset_id = rsp['photoset']['id']
        dump_progress(state)

    files = get_all_files()
    names = list(map(get_name, files))
    assert len(names) == len(set(names))

    for file in tqdm.tqdm(files, mininterval=1e-6):
        name = get_name(file)

        if name not in state.photos:
            photo_id = upload_photo(flickr, file, name)
            state.photos[name] = Photo(photo_id)
            dump_progress(state)

        photo = state.photos[name]
        if not photo.in_photoset:
            rsp = json.loads(flickr.photosets.addPhoto(
                photoset_id=state.photoset_id,
                photo_id=photo.photo_id,
                format='json',
            ))
            assert rsp['stat'] == 'ok'
            photo.in_photoset = True
            dump_progress(state)

    rsp = json.loads(flickr.photosets.getPhotos(
        photoset_id=state.photoset_id,
        extras=','.join(KEYS_TO_SAVE),
        format='json',
    ))
    assert rsp['stat'] == 'ok'

    photos = {photo['id']: {
        key: photo[key] for key in KEYS_TO_SAVE}
              for photo in rsp['photoset']['photo']}

    json.dump({
        photo_path: photos[photo.photo_id] for photo_path, photo in state.photos.items()
    }, open(OUTPUT_PATH, 'w'), ensure_ascii=False)


if __name__ == '__main__':
    main()
